from aws import AmazonWebServers
import os
import time
import threading

aws_access = ""
aws_secret = ""
key = ""

class LaunchInstances(threading.Thread):
    def __init__(self, id, aws, key, instance):
        threading.Thread.__init__(self)
        self.id = id
        self.aws = aws
        self.key = key
        self.instance = instance

    def run(self):
        print("thread %s starting" % (str(self.id)))

        cfg = {
            "ImageId": "ami-024a64a6685d05041", # Ubuntu Server 18.04 LTS (HVM)
            "InstanceType": self.instance["type"],
            "MaxCount": self.instance["count"],
            "MinCount": self.instance["count"],
            "BlockDeviceMappings": [
                {
                    "DeviceName": "/dev/sda1",
                    "Ebs": {
                        "Encrypted": False,
                        "DeleteOnTermination": True,
                        "VolumeSize": 10,
                        "VolumeType": "gp2"
                    }
                }
            ],
            "SecurityGroupIds": ["sg-019441bd802c8d8db"], # launch-wizard-jeferson
            "KeyName": self.key.split("/")[-1].split(".")[0]
        }

        # launch instances
        ids = aws.create_instances(cfg)
        time.sleep(100)

        # host
        aws.host(ids, str(self.id))

        # ssh
        os.system("ssh-keygen -f id_rsa_aws" + str(self.id) + " -P ''")
        files = {
            "id_rsa_aws" + str(self.id): "/home/ubuntu/id_rsa_aws",
            "id_rsa_aws" + str(self.id) + ".pub": "/home/ubuntu/id_rsa_aws.pub",
        }
        aws.upload_files(ids, self.key, files)
        commands = [
            "cp /home/ubuntu/id_rsa_aws /home/ubuntu/.ssh/id_rsa",
            "chmod 400 /home/ubuntu/.ssh/id_rsa",
            "cat /home/ubuntu/id_rsa_aws.pub >> /home/ubuntu/.ssh/authorized_keys",
        ]
        aws.execute_commands(ids, self.key, commands)

        # instance
        files = {
            "hosts" + str(self.id): "/home/ubuntu/hosts",
            "hostfile" + str(self.id): "/home/ubuntu/hostfile",
            "instance.sh": "/home/ubuntu/instance.sh",
            "Singularity": "/home/ubuntu/Singularity",
            "TBL/TBL3D.py": "/home/ubuntu/TBL3D.py",
            "TBL/umean.dat": "/home/ubuntu/umean.dat",
            "TBL/uumean.dat": "/home/ubuntu/uumean.dat",
            "TBL/uvmean.dat": "/home/ubuntu/uvmean.dat",
            "TBL/vvmean.dat": "/home/ubuntu/vvmean.dat",
            "TBL/wwmean.dat": "/home/ubuntu/wwmean.dat",
            "TBL/equation_library.py": "/home/ubuntu/equation_library.py",
        }
        aws.upload_files(ids, self.key, files)
        commands = [
            "cat hosts >> /etc/hosts",
            "chmod +x /home/ubuntu/instance.sh",
            "/home/ubuntu/instance.sh"
        ]
        aws.execute_commands(ids, self.key, commands)

        # run
        for run in range(3):
            n = self.instance["cpu"] * self.instance["count"]
            start = time.time()
            commands = [
                "time mpirun -n " + str(n) + " --hostfile hostfile singularity exec pyranda.simg python TBL3D.py"
            ]
            stdout, stderr = aws.execute_commands([ids[0]], self.key, commands)
            delta = time.time() - start
            print(delta)

            with open("%s_%s_%s_%s.txt" % (self.instance["type"], self.instance["cpu"], self.instance["count"], str(run)), "w+") as log:
                log.write(str(delta))
                log.write("\n\nstdout\n")
                log.write("".join(stdout))
                log.write("\n\nstderr\n")
                log.write("".join(stderr))

        # terminate instances
        aws.terminate_instances(ids)

        print("thread %s finished" % (str(self.id)))

aws = AmazonWebServers(aws_access, aws_secret)

instances = [
    {"type": "c5.large", "cpu": 2, "count": 1},
    {"type": "c5.large", "cpu": 2, "count": 2},
    {"type": "c5.large", "cpu": 2, "count": 4},
    {"type": "c5.xlarge", "cpu": 4, "count": 1},
    {"type": "c5.xlarge", "cpu": 4, "count": 2},
    {"type": "c5.2xlarge", "cpu": 8, "count": 1},
]

threads = []

for i, instance in enumerate(instances):
    thread = LaunchInstances(i, aws, key, instance)
    thread.start()
    threads.append(thread)

for t in threads:
    t.join()

print ("Exiting Main Thread")
