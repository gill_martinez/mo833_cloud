import boto3
import paramiko

class AmazonWebServers():
    def __init__(self, access, secret):
        self.ec2 = boto3.resource("ec2", region_name="us-east-1",
            aws_access_key_id=access,
            aws_secret_access_key=secret
        )
        self.client = boto3.client("ec2", region_name="us-east-1", 
            aws_access_key_id=access,
            aws_secret_access_key=secret
        )

    def create_instances(self, cfg):
        print("creating instances")
        instances = self.ec2.create_instances(**cfg)
        ids = [instance.id for instance in instances]
        waiter = self.client.get_waiter('instance_running')
        waiter.wait(InstanceIds=ids)
        print("instances created")
        return ids

    def upload_files(self, ids, key, files):
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print("uploading files")
        for id in ids:
            print(id + " uploading")
            ip = self.ec2.Instance(id).public_ip_address
            ssh_client.connect(hostname=ip, username="ubuntu", key_filename=key)
            ftp_client = ssh_client.open_sftp()
            for file in files:
                print("uploading: " + file)
                print("to: " + files[file])
                ftp_client.put(str(file), str(files[file]))
            ftp_client.close()
            ssh_client.close()
            print(id + " uploaded")

    def execute_commands(self, ids, key, commands, verbose=False):
        output = []
        output_err = []

        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print("executing commands")
        for id in ids:
            print(id + " executing")
            ip = self.ec2.Instance(id).public_ip_address
            ssh_client.connect(hostname=ip, username="ubuntu", key_filename=key)
            for command in commands:
                print("executing: " + command)
                stdin, stdout, stderr = ssh_client.exec_command(command)
                out = stdout.readlines()
                output += out
                err = stderr.readlines()
                output_err += err
                if verbose:
                    print("stdout:")
                    print("".join(out))
                    print("stderr:")
                    print("".join(err))
            ssh_client.close()
            print(id + " executed")
        return output, output_err

    def download_file(self, id, key, remote_path, local_path):
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ip = self.ec2.Instance(id).public_ip_address
        ssh_client.connect(hostname=ip, username="ubuntu", key_filename=key)
        ftp_client = ssh_client.open_sftp()
        print(id + " downloading: " + remote_path)
        print(id + " to: " + local_path)
        ftp_client.get(remote_path, local_path)
        ftp_client.close()
        ssh_client.close()

    def stop_instances(self, ids):
        print("stopping instances")
        self.client.stop_instances(InstanceIds=ids)
        waiter = self.client.get_waiter("instance_stopped")
        waiter.wait(InstanceIds=ids)
        print("instances stopped")

    def start_instances(self, ids):
        print("starting instances")
        self.client.start_instances(InstanceIds=ids)
        waiter = self.client.get_waiter("instance_running")
        waiter.wait(InstanceIds=ids)
        print("instances started")

    def terminate_instances(self, ids):
        print("terminating instances")
        self.client.terminate_instances(InstanceIds=ids)
        waiter = self.client.get_waiter("instance_terminated")
        waiter.wait(InstanceIds=ids)
        print("instances terminated")

    def host(self, ids, name):
        public_ips = [self.ec2.Instance(id).public_ip_address for id in ids]
        private_ips = [self.ec2.Instance(id).private_ip_address for id in ids]
        hostnames = ["ip-" + str(self.ec2.Instance(id).private_ip_address).replace(".", "-") for id in ids]
        with open("hosts" + name, "w") as hosts:
            for host in zip(private_ips, hostnames):
                hosts.write(" ".join(host) + "\n")
            for host in zip(public_ips, hostnames):
                hosts.write(" ".join(host) + "\n")
        with open("hostfile" + name, "w") as hostfile:
            for hostname in hostnames:
                hostfile.write(hostname + "\n")
