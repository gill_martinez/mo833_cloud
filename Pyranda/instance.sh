function wait_for_dpkg_lock {
  sudo lsof /var/lib/dpkg/lock > /dev/null
  dpkg_is_locked="$?"
  echo $dpkg_is_locked
  if [ "$dpkg_is_locked" == "0" ]; then
    echo "Waiting for another installation to finish"
    sleep 5
    wait_for_dpkg_lock
  fi
}

# requirements
wait_for_dpkg_lock
sudo apt update && sudo apt install -y build-essential libarchive-dev python openmpi-bin
wait_for_dpkg_lock

# singularity
VERSION=2.5.2
wget https://github.com/singularityware/singularity/releases/download/$VERSION/singularity-$VERSION.tar.gz
tar xvf singularity-$VERSION.tar.gz
cd singularity-$VERSION
./configure --prefix=/usr/local
make -j
sudo make install
cd ../ && rm singularity-2.5.2.tar.gz

# pyranda image
sudo singularity build pyranda.simg Singularity
