#bash function that checks the lock file before running the installer
function wait_for_dpkg_lock {
  # check for a lock on dpkg (another installation is running)
  sudo lsof /var/lib/dpkg/lock > /dev/null
  dpkg_is_locked="$?"
  echo $dpkg_is_locked
  if [ "$dpkg_is_locked" == "0" ]; then
    echo "Waiting for another installation to finish"
    sleep 5
    wait_for_dpkg_lock
  fi
}

#Download and build singularity
wget https://github.com/singularityware/singularity/releases/download/2.5.2/singularity-2.5.2.tar.gz
tar xvf singularity-2.5.2.tar.gz
cd singularity-2.5.2
wait_for_dpkg_lock
sudo apt-get update && sudo apt-get install build-essential libarchive-dev python -y
wait_for_dpkg_lock
./configure --prefix=/usr/local
make -j
sudo make install
cd ../ && rm singularity-2.5.2.tar.gz

#check if Gromacs singularity image exists
FILE=/home/ubuntu/mo833_cloud/Gromacs/gromacs.simg

if test -f "$FILE"; then
    echo "$FILE exist"
else
    cd mo833_cloud/Gromacs && sudo singularity build gromacs.simg Singularity      
fi
