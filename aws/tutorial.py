from aws import AmazonWebServers

aws = AmazonWebServers("my_access", "my_secret")

cfg = {
    "ImageId": "ami-0bc213dc857866046",
    "InstanceType": "c5.xlarge",
    "MaxCount": 1,
    "MinCount": 1,
    "Placement": {"GroupName": "pg-fernandes-cluster"},
    "BlockDeviceMappings": [
        {
            "DeviceName": "/dev/sda1",
            "Ebs": {
                "Encrypted": False,
                "DeleteOnTermination": True,
                "VolumeSize": 100,
                "VolumeType": "gp2"
            }
        }
    ],
    "SecurityGroupIds": ["sg-019441bd802c8d8db"],
    "KeyName": "fernandes",
    "UserData": "#!/bin/bash\ntouch a\n touch b",
}


key = "fernandes.pem"

# ids = aws.create_instances(cfg)
ids = ["i-050ed233937edcb75"]

files = {
    "test2.py": "/home/ubuntu/test2.py",
    "test2.py": "/home/ubuntu/test3.py"
}
aws.upload_files(ids, key, files)

commands = ["sudo mv ~/test2.py ~/test.py"]
aws.execute_commands(ids, key, commands)

commands = [
    "sudo mv ~/test3.py ~/test4.py",
    "ls ~"
]
stdout, stderr = aws.execute_commands([ids[0]], key, commands)
print(stdout)
print(stderr)

aws.download_file(ids[0], key, "/home/ubuntu/test4.py", "olar.py")

aws.stop_instances(ids)

aws.start_instances(ids)

commands = ["ls ~"]
stdout, stderr = execute_commands([ids[0]], key, commands)
print(stdout)
print(stderr)

aws.terminate_instances(ids)
