import boto3
import paramiko

class AmazonWebServers():
    def __init__(self, access, secret):
        self.ec2 = boto3.resource("ec2", region_name="us-east-1",
            aws_access_key_id=access,
            aws_secret_access_key=secret
        )
        self.client = boto3.client("ec2", region_name="us-east-1", 
            aws_access_key_id=access,
            aws_secret_access_key=secret
        )

    def create_instances(self, cfg):
        print("creating instances")
        instances = self.ec2.create_instances(**cfg)
        ids = [instance.id for instance in instances]
        waiter = self.client.get_waiter('instance_running')
        waiter.wait(InstanceIds=ids)
        print("instances created")
        return ids

    def upload_files(self, ids, key, files):
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print("uploading files")
        for id in ids:
            print(id)
            ip = self.ec2.Instance(id).public_ip_address
            ssh_client.connect(hostname=ip, username="ubuntu", key_filename=key)
            ftp_client = ssh_client.open_sftp()
            for file in files:
                print("uploading file: %s" % file)
                print("to: %s" % files[file])
                ftp_client.put(str(file), str(files[file]))
            ftp_client.close()
            ssh_client.close()
            print("upload success on %s!" % str(ip))

    def execute_commands(self, ids, key, commands, verbose=False):
        output = []
        output_err = []

        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print("executing commands")
        for id in ids:
            print(id)
            ip = self.ec2.Instance(id).public_ip_address
            ssh_client.connect(hostname=ip, username="ubuntu", key_filename=key)
            for command in commands:
                print("\nexecuting command: %s" % command)
                stdin, stdout, stderr = ssh_client.exec_command(command)
                out = stdout.readlines()
                output.append(out)
                err = stderr.readlines()
                output_err.append(err)
                if verbose:
                    print "stdout:"
                    print "".join(out)
                    print "stderr:"
                    print "".join(err)
            ssh_client.close()
            print("successeful execution on %s!" % str(ip))
        return output, output_err

    def download_file(self, id, key, remote_path, local_path):
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print(id)
        ip = self.ec2.Instance(id).public_ip_address
        ssh_client.connect(hostname=ip, username="ubuntu", key_filename=key)
        print("downloading file")
        ftp_client = ssh_client.open_sftp()
        print("downloading file: %s" % remote_path)
        print("to: %s" % local_path)
        ftp_client.get(remote_path, local_path)
        ftp_client.close()
        ssh_client.close()

    def stop_instances(self, ids):
        print("stopping instances")
        client.stop_instances(InstanceIds=ids)
        waiter = client.get_waiter("instance_stopped")
        waiter.wait(InstanceIds=ids)
        print("instances stopped")

    def start_instances(self, ids):
        print("starting instances")
        self.client.start_instances(InstanceIds=ids)
        waiter = self.client.get_waiter("instance_running")
        waiter.wait(InstanceIds=ids)
        print("instances started")

    def terminate_instances(self, ids):
        print("terminating instances")
        self.client.terminate_instances(InstanceIds=ids)
        waiter = self.client.get_waiter("instance_terminated")
        waiter.wait(InstanceIds=ids)
        print("instances terminated")
