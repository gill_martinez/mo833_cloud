from aws import AmazonWebServers
import sys
import threading
import time 


############# Modifique somente esta parte do Arquivo ##################

key             = 'path/to/key.pem'
accessKeyID     = ''
secretAccessKey = ''

#####################################################################



class LaunchInstances(threading.Thread):
    def __init__(self, threadID, instanceName, aws, instanceType):
        threading.Thread.__init__(self)
        self.threadID = threadID 
        self.instance = instanceName
        self.aws = aws
        self.type = instanceType
        self.ids = None 

    def run(self):
        print("Thread %s Starting %s instance %s \n" % (str(self.threadID), self.instance, self.type ))
        cfg = []
        commands = []
        
        #extract just the keyName from Key
        keyName = key.split("/")[-1].split(".")[0]
        
        #set config cfg
        cfg = {
            "ImageId": "ami-05ca11a79fafd6ebe", #Deep Learning Base AMI (Ubuntu) Version 18.0 
            "InstanceType": self.instance,
            "MaxCount": 1,
            "MinCount": 1,
            "BlockDeviceMappings": [
                {
                    "DeviceName": "/dev/sda1",
                    "Ebs": {
                        "Encrypted": False,
                        "DeleteOnTermination": True,
                        "VolumeSize": 75,
                        "VolumeType": "gp2"
                    }
                }
            ],
            "SecurityGroupIds": ["sg-019441bd802c8d8db"],
            "KeyName": keyName,
            "UserData": "#!/bin/bash\nsudo apt-get update && sudo apt-get install build-essential libarchive-dev python -y", 
        }
        
        #create instance
        self.ids = aws.create_instances(cfg)
        #ids = ["i-070e2cf809be5510f"]
        #aws.start_instances(ids)
        
        #wait for aws create and update packages 
        print("waiting %s instance update and install required packages ..." % self.instance)
        time.sleep(100)

        if self.type == 'GPU':
            commands = ["git clone https://gill_martinez@bitbucket.org/gill_martinez/mo833_cloud.git",
                        "bash mo833_cloud/init.bash",
                        "bash mo833_cloud/run_gromacs.bash GPU 3"]
        else:
            commands = ["git clone https://gill_martinez@bitbucket.org/gill_martinez/mo833_cloud.git",
                        "bash mo833_cloud/init.bash",
                        "bash mo833_cloud/run_gromacs.bash CPU 3"]
        
        #execute commands 
        # for verbose output -> verbose = True
        stdout, stderr = aws.execute_commands([self.ids[0]], key, commands, verbose=True)

        #download result file
        aws.download_file(self.ids[0], key, "/home/ubuntu/mo833_cloud/Gromacs/output/time.log", "../Gromacs/output/%s" % self.instance)
        
        if self.type == 'GPU':
            aws.download_file(self.ids[0], key, "/home/ubuntu/mo833_cloud/Gromacs/output/gpu.log", "../Gromacs/output/gpu_%s" % self.instance)
            
        #destroy instance
        aws.terminate_instances(self.ids)
        
        print("\nInstance %s finished\n\n" % self.instance)


instances = {'c5.xlarge' : {"type": "CPU"},
            'c5.2xlarge' : {"type": "CPU"},
            'c5.4xlarge' : {"type": "CPU"},
            'c5.9xlarge' : {"type": "CPU"},
            'g3s.xlarge' : {"type": "GPU"},
            'g3.4xlarge' : {"type": "GPU"},
            'p2.xlarge'  : {"type": "GPU"},
            'p3.2xlarge' : {"type": "GPU"}}


if __name__ == "__main__":

    #Init Aws Web Services
    aws = AmazonWebServers(accessKeyID, secretAccessKey)

    threads = []
    
    #launch one thread per instance
    i = 0
    for instance, items in instances.items():
        thread = LaunchInstances(i, instance, aws, items["type"])
        thread.start()
        threads.append(thread)
        i+=1

    #wait all threads cpu to finish
    for t in threads:
        t.join()
     
    print ("\n\n Exiting Main Thread.. Run gromacs_charts for results ! \n")