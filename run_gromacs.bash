TYPE=$1
TIMES=$2

cd mo833_cloud/Gromacs && mkdir -p output/  
cd input/files

if [ "$TYPE" = "CPU" ]; then
    echo "Running Gromacs CPU only."
    for i in {0.. "$TIMES" }; 
    do 
        singularity run --app CPU ../../gromacs.simg
        awk '/Time/ {print $3;}' gromacs_cpu.log >> ../../output/time.log
    done
    
else
    echo "Running Gromacs with GPU support."
    #just for CUDA Runtime problems
    nvcc vecAdd.cu -o vecAdd
    ./vecAdd
    rm vecAdd
    for i in {0.. "$TIMES" }; 
    do 
        singularity run --nv --app GPU ../../gromacs.simg
        awk '/Time/ {print $3;}' gromacs.log >> ../../output/time.log
        mv gpu.log ../../output/ 

    done    
fi