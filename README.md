# MO833 Cloud 

Final Project - MO833 @2019

Institute of Computing - Unicamp 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes. 

### Prerequisites

1. Amazon Access key ID
2. Secret access key
3. private key file
4. python & pip 
  
```
sudo apt-get install python python-pip 
```

### Installing

 - required libraries

```
cd aws/ && sudo pip install -r requirements.txt
```

## Running the tests

### Gromacs 


1. ```
    cd aws/
    ```

2. open gromacs_benchmark_threads.py file and insert your keyName, accessKeyID and secretAccessKey at the demarcated locations. 

3. ```
    python gromacs_benchmark_threads.py
    ```

The python script will create the instances, run the tests, download the output results and kill the instances. This step can takes ~ 40 minutes. Now we can run the jupyter-notebook to plot the results.

4. ```
    jupyter-notebook 
    ```

By default, jupyter will be running at http://localhost:8888 

5. open gromacs_charts.ipynb; select "Kernel" tab --> "Restart & Run All"

The results should be displayed at your browser.

### Pyranda

1. ```
    cd Pyranda/
    ```

2. open pyranda.py file and insert your keyName, accessKeyID and secretAccessKey at the demarcated locations. 

3. ```
    python pyranda.py
    ```

The python script will create the instances, run the tests, download the output results and kill the instances. This step can takes ~ 50 minutes. Now we can run the jupyter-notebook to plot the results.

4. ```
    jupyter-notebook 
    ```

By default, jupyter will be running at http://localhost:8888 

5. open pyranda_charts.ipynb; select "Kernel" tab --> "Restart & Run All"

The results should be displayed at your browser.

## Authors

* **Gilberto Martinez Jr**  

* **Matheus F. Sarmento**

## Professor

* **Edson Borin**

## License

This project is licensed under the GNU LESSER GENERAL PUBLIC LICENSE - see the [LICENSE](LICENSE) file for details


